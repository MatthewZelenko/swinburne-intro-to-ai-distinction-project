﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private static GameManager m_instance = null;
    public AIManager m_aiManager;
    public LevelManager m_levelManager;
    public SpriteManager m_spriteManager;

    public List<Factory> m_factoryObjects;
    public Dictionary<Type, Factory> m_factories;

    public static GameManager GetInstance()
    {
        return m_instance;
    }

    void Awake()
    {
        if (m_instance)
        {
            DestroyImmediate(gameObject);
            return;
        }
        m_instance = this;
        DontDestroyOnLoad(this);

        m_aiManager = new AIManager();
        m_aiManager.Init();

        m_spriteManager = new SpriteManager();
        m_factories = new Dictionary<Type, Factory>();
        for (int i = 0; i < m_factoryObjects.Count; i++)
        {
            m_factories.Add(m_factoryObjects[i].GetType(), m_factoryObjects[i]);
        }
    }
    private void Start()
    {
        m_levelManager.CreateLevel();
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            Time.timeScale = 1;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            Time.timeScale = 10;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            Time.timeScale = 20;
        }
    }
    void FixedUpdate()
    {
        foreach (Factory f in m_factories.Values)
        {
            f.CheckDeadEntities();
        }
        m_aiManager.FixedUpdate();
    }

    public T GetFactory<T>() where T : Factory
    {
        return m_factories[typeof(T)] as T;
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class SpriteManager
{
    Dictionary<string, Dictionary<string, Sprite>> m_spriteSheets;

    public SpriteManager()
    {
        m_spriteSheets = new Dictionary<string, Dictionary<string, Sprite>>();
    }

    public bool LoadSpriteSheet(string a_spritesheetName)
    {
        Sprite[] sprites = Resources.LoadAll<Sprite>("Sprites/" + a_spritesheetName);
        if (sprites != null && sprites.Length != 0)
        {
            m_spriteSheets[a_spritesheetName] = new Dictionary<string, Sprite>();
            for (int i = 0; i < sprites.Length; i++)
            {
                m_spriteSheets[a_spritesheetName][sprites[i].name] = sprites[i];
            }
            return true;
        }
        return false;
    }
    public Sprite GetSprite(string a_spritesheetName, string a_spriteName)
    {
        if (!m_spriteSheets.ContainsKey(a_spritesheetName))
            if (!LoadSpriteSheet(a_spritesheetName))
                return null;

        if (m_spriteSheets[a_spritesheetName].ContainsKey(a_spriteName))
            return m_spriteSheets[a_spritesheetName][a_spriteName];
        return null;
    }
}
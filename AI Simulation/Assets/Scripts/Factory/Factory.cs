﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public abstract class Factory : MonoBehaviour
{
    public List<Entity> m_entities;

    public virtual Entity CreateEntity(Entity a_prefab)
    {
        Entity newEntity = GameObject.Instantiate(a_prefab);
        m_entities.Add(newEntity);
        newEntity.m_factoryID = m_entities.Count - 1;
        return newEntity;
    }
    void DestroyEntity(Entity a_entity)
    {
        a_entity.Clear();
        //TODO:Gravestone sprite? lol
        GameObject.Destroy(a_entity.gameObject);
    }
    public void CheckDeadEntities()
    {
        List<Entity> newList = new List<Entity>();
        for (int i = m_entities.Count - 1; i >= 0; i--)
        {
            if (m_entities[i].Dead)
            {
                DestroyEntity(m_entities[i]);
            }
            else
            {
                newList.Add(m_entities[i]);
            }
        }
        m_entities = newList;
    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class EatAction : AIAction
{
    Warehouse m_warehouse;

    public EatAction(Villager a_entity) : base(a_entity)
    {
    }

    public void Init()
    {
        m_warehouse = GameManager.GetInstance().GetFactory<HouseFactory>().GetWarehouse();
    }
    public override IEnumerator Run()
    {
        Init();
        if (Parent.GetStat(Villager.StatTypes.HUNGER) == 100)
            yield break;

        yield return RunAction(new MoveToAction(m_warehouse, Parent));
        if (Parent.m_inventoryComponent.HasResources)
        {
            yield return RunAction(new GiveAllResourcesAction(m_warehouse.m_inventoryComponent, 1.0f, Parent));
        }
        //TODO::Depending on hunger level, take more food
        Parent.SetSprite("Villager_Eating");
        yield return RunAction(new TakeResourceAction(m_warehouse.m_inventoryComponent, Resource.ResourceType.Food, 1, 6.0f, Parent));
        if (Parent.m_inventoryComponent.TakeResource(Resource.ResourceType.Food).Count != 0)
        {
            Parent.AddStat(Villager.StatTypes.HUNGER, 60);
        }
        Clear();
    }

    protected override void Clean()
    {
    }
}
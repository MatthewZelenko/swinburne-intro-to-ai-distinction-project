﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WanderMode : AIAction
{
    float m_duration, m_elapseDuration;
    float m_speed;

    public WanderMode(float a_minPause, float a_maxPause, float a_minWalk, float a_maxWalk, Villager a_entity) : base(a_entity)
    {
        m_elapseDuration = 0.0f;
        m_duration = 0.0f;
        m_speed = 0.0f;

        CurrentDirection = Random.Range(-1, 2);
        if (CurrentDirection != 0)
        {
            m_duration = Random.Range(a_minWalk, a_maxWalk);
            m_speed = CurrentDirection * Parent.m_movementSpeed * Time.fixedDeltaTime;
        }
        else
        {
            m_duration = Random.Range(a_minPause, a_maxPause);
        }
    }

    public void Init()
    {
        if (!Parent.m_inventoryComponent.HasResources)
        {
            Parent.SetSprite("Villager_Idle");
        }
    }
    public override IEnumerator Run()
    {
        Init();
        while (m_elapseDuration < m_duration)
        {
            m_elapseDuration += Time.fixedDeltaTime;
            Parent.transform.Translate(m_speed, 0, 0);
            yield return new WaitForFixedUpdate();
        }
        Clear();
    }

    protected override void Clean()
    {
    }

    int CurrentDirection { get; set; }
}
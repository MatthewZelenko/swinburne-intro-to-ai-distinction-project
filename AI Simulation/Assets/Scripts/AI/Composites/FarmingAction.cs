﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class FarmingAction : AIAction
{
    Farm m_targetFarm;
    Warehouse m_warehouse;
    public FarmingAction(Villager a_entity) : base(a_entity)
    {
    }

    private void Init()
    {
        m_warehouse = GameManager.GetInstance().GetFactory<HouseFactory>().GetWarehouse();
        m_targetFarm = GameManager.GetInstance().GetFactory<FarmFactory>().GetAvailableFarm();
        if (m_targetFarm == null)
            return;
        m_targetFarm.m_residentComponent.AddResident(Parent);
    }

    public override IEnumerator Run()
    {
        Init();

        //1. Check if farm is not built
        //  1a. Go to warehouse if do not have required resources.
        //2. Go to farm
        //3. Hoe farm
        //4. Take food resource back to warehouse
        //5. Done


        if (m_targetFarm == null || m_warehouse == null)
        {
            Clear();
            yield break;
        }

        if (!m_targetFarm.m_buildComponent.IsBuilt)
        {
            if (!Parent.m_inventoryComponent.HasResources || (Parent.m_inventoryComponent.HasResources && Parent.m_inventoryComponent.PeekAtResource().TypeOfResource != Resource.ResourceType.Wood))
            {
                yield return RunAction(new MoveToAction(m_warehouse, Parent));
                yield return RunAction(new GiveAllResourcesAction(m_warehouse.m_inventoryComponent, 2, Parent));
                yield return RunAction(new TakeResourceAction(m_warehouse.m_inventoryComponent, Resource.ResourceType.Wood, 1, 1, Parent));
            }
            if (Parent.m_inventoryComponent.HasResources)
            {
                yield return RunAction(new MoveToAction(m_targetFarm, Parent));
                if (Mathf.Abs(m_targetFarm.transform.position.x - Parent.transform.position.x) <= 1)
                {
                    yield return RunAction(new BuildAction(m_targetFarm.m_buildComponent, Resource.ResourceType.Wood, 1, 3.0f, Parent));
                    Parent.AddStat(Villager.StatTypes.JOBQUOTA, 25);
                }
            }
        }
        else
        {
            yield return RunAction(new MoveToAction(m_targetFarm, Parent));
            Parent.SetSprite("Villager_Farming");
            yield return RunAction(new TakeResourceAction(m_targetFarm.m_inventoryComponent, Resource.ResourceType.Food, 1, 5, Parent));
            Parent.SetSprite("Villager_Hold_Food");
            yield return RunAction(new MoveToAction(m_warehouse, Parent));
            yield return RunAction(new GiveAllResourcesAction(m_warehouse.m_inventoryComponent, 2, Parent));
            Parent.AddStat(Villager.StatTypes.JOBQUOTA, 25);
        }
        Clear();
    }

    protected override void Clean()
    {
        if (m_targetFarm != null)
        {
            m_targetFarm.m_residentComponent.RemoveResident(Parent);
            m_targetFarm = null;
        }
        m_warehouse = null;
    }
}
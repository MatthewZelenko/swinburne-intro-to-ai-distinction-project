﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class LumberjackAction : AIAction
{
    Tree m_targetTree;
    Warehouse m_warehouse;

    public LumberjackAction(Villager a_entity) : base(a_entity)
    {
    }

    public void Init()
    {
        m_warehouse = GameManager.GetInstance().GetFactory<HouseFactory>().GetWarehouse();
        m_targetTree = GameManager.GetInstance().GetFactory<ForestFactory>().GetUnoccupiedTree();
        if (m_targetTree == null)
            return;
        m_targetTree.m_residentComponent.AddResident(Parent);
    }
    public override IEnumerator Run()
    {
        //TODO: Add new actions after each action.
        //1. Do you have wood?
        //2. No, Walk to warehouse
        //3. Get wood
        //4. Walk to House
        //5. Build House
        //6. Done

        Init();
        if (m_targetTree == null || m_warehouse == null)
        {
            Clear();
            yield break;
        }

        //Get tree if haven't already
        if (!Parent.m_inventoryComponent.HasResources)
        {
            yield return RunAction(new MoveToAction(m_targetTree, Parent));
            Parent.SetSprite("Villager_Chop");
            yield return RunAction(new TakeResourceAction(m_targetTree.RetrieveComponent<InventoryComponent>(), Resource.ResourceType.Wood, 1, 5, Parent));
        }
        if (Parent.m_inventoryComponent.PeekAtResource(Resource.ResourceType.Wood).Count == 0)
        {
            Clear();
            yield break;
        }
        
        yield return RunAction(new MoveToAction(m_warehouse, Parent));
        yield return RunAction(new GiveAllResourcesAction(m_warehouse.m_inventoryComponent, 2, Parent));
        Parent.AddStat(Villager.StatTypes.JOBQUOTA, 25);
        Clear();
    }
    protected override void Clean()
    {
        if (m_targetTree != null)
        {
            m_targetTree.m_residentComponent.RemoveResident(Parent);
            m_targetTree = null;
        }
        m_warehouse = null;
    }
}
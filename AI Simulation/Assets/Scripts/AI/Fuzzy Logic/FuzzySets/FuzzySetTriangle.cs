﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class FuzzySetTriangle : FuzzySet
{
    double m_peakPoint, m_leftOffset, m_rightOffset;

    public FuzzySetTriangle(double a_mid, double a_left, double a_right) : base(a_mid)
    {
        m_peakPoint = a_mid;
        m_leftOffset = a_left;
        m_rightOffset = a_right;
    }

    public override double CalculateDOM(double a_value)
    {
        if (m_peakPoint == a_value && (m_rightOffset == 0.0 || m_leftOffset == 0.0f))
            return 1.0;

        if (a_value <= m_peakPoint && a_value >= m_peakPoint - m_leftOffset)
        {
            double grad = 1.0 / m_leftOffset;
            return grad * (a_value - m_peakPoint - m_leftOffset);
        }
        else if (a_value > m_peakPoint && a_value < m_peakPoint + m_rightOffset)
        {
            double grad = 1.0f / -m_rightOffset;
            return grad * (a_value - m_peakPoint) + 1.0;
        }
        else return 0.0;
    }
}
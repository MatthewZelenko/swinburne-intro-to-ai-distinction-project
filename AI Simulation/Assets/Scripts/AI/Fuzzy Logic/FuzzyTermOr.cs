﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class FuzzyTermOr : FuzzyTerm
{
    List<FuzzyTerm> m_terms;

    public FuzzyTermOr(FuzzyTerm a_op1, FuzzyTerm a_op2)
    {
        m_terms = new List<FuzzyTerm>();
        m_terms.Add(a_op1);
        m_terms.Add(a_op2);
    }
    public FuzzyTermOr(FuzzyTerm a_op1, FuzzyTerm a_op2, FuzzyTerm a_op3)
    {
        m_terms = new List<FuzzyTerm>();
        m_terms.Add(a_op1);
        m_terms.Add(a_op2);
        m_terms.Add(a_op3);
    }
    public FuzzyTermOr(FuzzyTerm a_op1, FuzzyTerm a_op2, FuzzyTerm a_op3, FuzzyTerm a_op4)
    {
        m_terms = new List<FuzzyTerm>();
        m_terms.Add(a_op1);
        m_terms.Add(a_op2);
        m_terms.Add(a_op3);
        m_terms.Add(a_op4);
    }


    public override double GetDOM()
    {
        double largest = double.MinValue;

        foreach (FuzzyTerm t in m_terms)
        {
            if (t.GetDOM() > largest)
            {
                largest = t.GetDOM();
            }
        }
        return largest;
    }

    public override void ORWithDOM(double a_value)
    {
    }
    public override void ClearDOM()
    {
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Component
{
    public Component(Entity a_entity)
    {
        Parent = a_entity;
    }

    public virtual void Init()
    {

    }
    public virtual void Update()
    {

    }
    public virtual void FixedUpdate()
    {

    }
    public virtual void Clear()
    {

    }

    public Entity Parent { get; private set; }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class ResidentComponent : Component
{
    int m_maxResidents;
    List<Villager> m_residents;

    BuildComponent m_buildComponent;

    public ResidentComponent(Entity a_entity, int a_maxResidents) : base(a_entity)
    {
        m_residents = new List<Villager>();
        m_maxResidents = a_maxResidents;
    }

    public override void Init()
    {
    }
    public override void Update()
    {
    }
    public override void FixedUpdate()
    {
    }
    public override void Clear()
    {
    }


    public bool AddResident(Villager a_villager)
    {
        if (Full || (m_buildComponent != null && !m_buildComponent.IsBuilt))
            return false;
        m_residents.Add(a_villager);
        return true;
    }
    public void RemoveResident(Villager a_villager)
    {
        for (int i = 0; i < m_residents.Count; i++)
        {
            if (m_residents[i] == a_villager)
            {
                m_residents.RemoveAt(i);
                return;
            }
        }
    }
    public void HasBuildComponent(BuildComponent a_component = null)
    {
        if (a_component != null)
            m_buildComponent = a_component;
        else
            m_buildComponent = Parent.RetrieveComponent<BuildComponent>();
    }


    public int NumberOfResidents { get { return m_residents.Count; } }

    public bool Full { get { return m_residents.Count >= m_maxResidents;  } }
}
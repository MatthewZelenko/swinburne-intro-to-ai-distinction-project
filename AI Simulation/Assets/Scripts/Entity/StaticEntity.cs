﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class StaticEntity : Entity
{
    public StaticEntity(int a_tileSize) : base()
    {
        TileSize = a_tileSize;
    }
    //Number of tiles this entity takes up
    public int TileSize { get; private set; }
    public List<int> OccupiedTiles { get; set; }
}